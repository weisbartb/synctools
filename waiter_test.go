package synctools

import (
	"math/rand"
	"testing"
	"time"
)

func TestWaitGroup_Add(t *testing.T) {
	group := WaitGroup{}
	num := rand.Int()
	group.Add(int64(num))
	if group.numActive != int64(num) {
		t.Errorf("Expected to see %v active items, saw %v instead", group.numActive, num)
	}
	group.Add(int64(num))
	if group.numActive != int64(num*2) {
		t.Errorf("Expected to see %v active items, saw %v instead", group.numActive, num*2)
	}
}
func TestWaitGroup_Done(t *testing.T) {
	group := WaitGroup{}
	num := rand.Int63n(10000)
	group.Add(num)
	if group.numActive != num {
		t.Errorf("Expected to see %v active items, saw %v instead", group.numActive, num)
	}
	for i := 0; i < int(num); i++ {
		group.Done()
	}
	if group.numActive != 0 {
		t.Errorf("Expected to see 0 items in group, saw %v", group.numActive)
	}
}
func TestWaitGroup_WaitUntil(t *testing.T) {
	wg := WaitGroup{}
	wg.Add(1)
	now := time.Now()
	till := now.Add(time.Second)
	if err := wg.WaitUntil(till); err != nil && err != ErrTimeoutOccurred {
		t.Error(err)
	}
	if time.Now().Sub(now) > time.Second*3 {
		t.Errorf("Excesive drift on timeout check detected")
	}
}

func TestWaitGroup_WaitFor(t *testing.T) {
	wg := WaitGroup{}
	wg.Add(1)
	now := time.Now()
	if err := wg.WaitFor(time.Second); err != nil && err != ErrTimeoutOccurred {
		t.Error(err)
	}
	if time.Now().Sub(now) > time.Second*3 {
		t.Errorf("Excesive drift on timeout check detected")
	}
}
