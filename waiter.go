package synctools

import (
	"sync"
	"sync/atomic"
	"time"

	"github.com/pkg/errors"
)

const defaultPollTime = time.Duration(time.Millisecond) // 1000hz

var ErrTimeoutOccurred = errors.New("timeout occurred")

type WaitGroup struct {
	numActive int64
	mu        sync.Mutex
}

func (wg *WaitGroup) Add(ct int64) int {
	return int(atomic.AddInt64(&wg.numActive, ct))
}
func (wg *WaitGroup) Done() {
	atomic.AddInt64(&wg.numActive, -1)
}

func (wg *WaitGroup) Wait() {
	for {
		if wg.numActive == 0 {
			return
		}
		time.Sleep(defaultPollTime)
	}
}
func (wg *WaitGroup) WaitInterval(hz time.Duration) {
	for {
		if wg.numActive == 0 {
			return
		}
		time.Sleep(hz)
	}
}

func (wg *WaitGroup) WaitUntil(t time.Time) error {
	waitTill := t.Sub(time.Now())
	if waitTill < 0 {
		return ErrTimeoutOccurred
	}
	timer := time.NewTimer(waitTill)
	for {
		select {
		case <-timer.C:
			return ErrTimeoutOccurred
		default:
			if wg.numActive == 0 {
				timer.Stop()
				return nil
			}
			time.Sleep(defaultPollTime)
		}
	}
}
func (wg *WaitGroup) WaitUntilWithHz(t time.Time, hz time.Duration) error {
	waitTill := t.Sub(time.Now())
	if waitTill < 0 {
		return ErrTimeoutOccurred
	}
	timer := time.NewTimer(waitTill)
	for {
		select {
		case <-timer.C:
			return ErrTimeoutOccurred
		default:
			if wg.numActive == 0 {
				timer.Stop()
				return nil
			}
			time.Sleep(hz)
		}
	}
}

func (wg *WaitGroup) WaitFor(duration time.Duration) error {
	timer := time.NewTimer(duration)
	for {
		select {
		case <-timer.C:
			return ErrTimeoutOccurred
		default:
			if wg.numActive == 0 {
				timer.Stop()
				return nil
			}
			time.Sleep(defaultPollTime)
		}
	}
}
func (wg *WaitGroup) WaitForWithHz(duration, hz time.Duration) error {
	timer := time.NewTimer(duration)
	for {
		select {
		case <-timer.C:
			return ErrTimeoutOccurred
		default:
			if wg.numActive == 0 {
				timer.Stop()
				return nil
			}
			time.Sleep(hz)
		}
	}
}
